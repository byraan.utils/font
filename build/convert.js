module.name=module.filename.replace(/^.*[\\\/]/, '').replace(/\..*$/, '');
require('../lib/command-line-args')(this, [
	{
		name: 'input',
		alias: 'i',
		type: String,
		defaultValue: ''
	},
	{
		name: 'output',
		alias: 'o',
		type: String,
		defaultValue: ''
	},
	{
		name: 'format',
		alias: 'f',
		type: String,
		defaultValue: 'woff',
		multiple: true
	}
	/* {
		name: 'file',
		alias: 'f',
		type: String,
		defaultValue: [],
		multiple: true
	},
	{
		name: 'to',
		type: String,
		defaultValue: [],
		multiple: true
	} */
], 1);
const path=require('path'),
fs=require('../lib/fs'),
log=require('../lib/log');
var support={
	otf: 'ttf',
	svg: 'ttf',
	ttf: [
		'eot',
		'woff',
		'woff2'
	]
};
var convertFont = require('converter-webfont');
process.arguments.input=path.resolve(process.arguments.input);
process.arguments.output=path.resolve(process.arguments.output);


//do something

try {
    convertFont(process.arguments.input, process.arguments.output, String(process.arguments.format));
} catch (err) {
    console.log(err);
}

//do something



/* for(let i=0; i<process.arguments.file.length; i++){
	let data=path.parse(process.arguments.file[i]);
	let ext=data.ext.slice(1);
	let format=support[ext];
	for(let j=0; j<process.arguments.to.length; j++){
		if(ext==process.arguments.to[j]) continue;
		if(typeof format!='object') format=[format];
		if(format.indexOf(process.arguments.to[j])<0){
			log(`{{warn}} conversion not supported: ´${data.base}´ to ´~.${process.arguments.to[j]}´`);
			continue;
		}
		let submod=`${ext}2${process.arguments.to[j]}`;
		let file=`${__dirname}/${module.name}/${submod}.js`;
		if(!fs.accessSync(file)){
			log(`{{error}} converter ´${submod}´ not found`);
			continue;
		}
		log(`{{info}} converting ´${data.base}´ to ´~.${process.arguments.to[j]}´`);
		require(file)(process.arguments.file[i]);
	}
} */