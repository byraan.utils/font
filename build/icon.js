const fs=require('fs'),
path=require('path'),
gulp=require('gulp'),
gulpClone=require('gulp-clone'),
iconfont=require('gulp-iconfont');
const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log.js');
commandLineArgs(this, [
	{
		name: 'input',
		alias: 'i',
		type: String,
		multiple: true,
		defaultValue: [],
		description: 'Icons folder that contains ´svg´ files'
	},
	{
		name: 'output',
		alias: 'o',
		type: String,
		defaultValue: './',
		description: 'Destination folder to save font files'
	},
	{
		name: 'css-rule',
		type: Boolean,
		defaultValue: false,
		description: 'Skip @font-face rule'
	},
	{
		name: 'class',
		type: String,
		description: 'Class prefix to use icons'
	},
	{
		name: 'prefix',
		type: String,
		defaultValue: ""
	},
	{
		name: 'no-css',
		type: Boolean,
		defaultValue: false
	},
	{
		name: 'format',
		alias: 'f',
		type: String,
		defaultValue: ['woff'],
		multiple: true,
		description: 'Save iconfont as many file types'
	}
], 1);
if(!this.subcommands) this.subcommands=['icons'];
process.arguments.name=this.subcommands[0];
if(!process.arguments.name) return log('{{error}} Falta nombre');
if(!process.arguments.class) process.arguments.class=process.arguments.name;
var runTimestamp=Math.round(Date.now()/1000),
support=['eot', 'woff2', 'woff', 'ttf'],
formats={
	ttf: 'truetype',
	eot: 'embedded-opentype'
};
gulp.task('default', function(cb){
	if(!process.arguments.input.length) process.arguments.input=[process.arguments.name];
	for(let i=0; i<process.arguments.input.length; i++){
		process.arguments.input[i]=path.resolve(process.arguments.input[i]);
		if(!fs.existsSync(process.arguments.input[i])) continue;
		if(fs.lstatSync(process.arguments.input[i]).isDirectory()) process.arguments.input[i]+='/*.svg';
	}
	var iconfonts=gulp.src(process.arguments.input)
	.pipe(iconfont({
		fontName: process.arguments.prefix+process.arguments.name,
		fileName: fontFamilyName,
		prependUnicode: false,
		formats: process.arguments.format,
		timestamp: runTimestamp,
		fontHeight: 1001,
		normalize: true
	})).on('glyphs', function(glyphs, options){
		if(process.arguments['no-css']) return;
		// console.log(glyphs, options)
		let css=createCss(glyphs, options);
		if(!fs.existsSync(process.arguments.output)) fs.mkdirSync(process.arguments.output, {recursive:true});
		fs.writeFileSync(`${process.arguments.output}/${fontFamilyName}.css`, css);
		return;
		for(let i=0; i<process.arguments.output.length; i++){
		}
	})
	.pipe(gulpClone())
	.pipe(gulp.dest(process.arguments.output));
	cb();
});
gulp.parallel(['default'])();

var indexIcon = process.arguments.class.indexOf('icon-');
var sliceIcon = process.arguments.class.slice(5);
var fontFamilyName = `icon${ indexIcon === 0 ? (sliceIcon ? '-' + sliceIcon : '') : (process.arguments.class == 'icon' ? '' : '-' + process.arguments.class)}`;

function createCss(glyphs, options){
	var css='/* https://gitlab.com/byraan/iconfont */\n';
	if(process.arguments['css-rule']==true){
		css+=`
@font-face{
	font-family: "${process.arguments.prefix}${fontFamilyName}";`;
	src: {
		let url='';
		css+=`
	src:`;
		for(let i=0; i<support.length; i++){
			if(process.arguments.format.indexOf(support[i])<0) continue;
			if(support[i]=='eot'){
				url+=`, url("${fontFamilyName}.${support[i]}?${runTimestamp}")`;
			}
			url+=`, url("${fontFamilyName}.${support[i]}?${runTimestamp}${support[i]=='eot'?'#iefix':''}") format("${formats[support[i]]?formats[support[i]]:support[i]}")`;
		}
		url=url.slice(1);
		css+=`${url};`;
	}
	css+=`
	font-weight: normal;
	font-style: normal;
}
`;
/* [class^="${fontFamilyName}-inherit-inherit-inherit-after-"]>*>*>*:after,
[class^="${fontFamilyName}-inherit-inherit-after-"]>*>*:after,
[class^="${fontFamilyName}-inherit-after-"]>*:after,
[class^="${fontFamilyName}-after-"]:after,
[class*=" ${fontFamilyName}-inherit-inherit-inherit-after-"]>*>*>*:after,
[class*=" ${fontFamilyName}-inherit-inherit-after-"]>*>*:after,
[class*=" ${fontFamilyName}-inherit-after-"]>*:after,
[class*=" ${fontFamilyName}-after-"]:after,
[class^="${fontFamilyName}-inherit-inherit-inherit-"]:not([class^="${fontFamilyName}-inherit-inherit-after-"])>*>*>*:before,
[class^="${fontFamilyName}-inherit-inherit-"]:not([class^="${fontFamilyName}-inherit-inherit-after-"])>*>*:before,
[class^="${fontFamilyName}-inherit-"]:not([class^="${fontFamilyName}-inherit-after-"])>*:before,
[class^="${fontFamilyName}-"]:not([class^="${fontFamilyName}-after-"]):before,
[class*=" ${fontFamilyName}-inherit-inherit-inherit-"]:not([class*=" ${fontFamilyName}-inherit-inherit-after-"])>*>*>*:before,
[class*=" ${fontFamilyName}-inherit-inherit-"]:not([class*=" ${fontFamilyName}-inherit-inherit-after-"])>*>*:before,
[class*=" ${fontFamilyName}-inherit-"]:not([class*=" ${fontFamilyName}-inherit-after-"])>*:before,
[class*=" ${fontFamilyName}-"]:not([class*=" ${fontFamilyName}-after-"]):before */
	}
		css+=`
[class^="${fontFamilyName}-after"]:not(td),
[class^="${fontFamilyName}-before"]:not(td),
[class*=" ${fontFamilyName}-after"]:not(td),
[class*=" ${fontFamilyName}-before"]:not(td)
{
	display: block;
}

[class^="${fontFamilyName}-after-"]:after,
[class^="${fontFamilyName}-before-"]:before,
[class*=" ${fontFamilyName}-after-"]:after,
[class*=" ${fontFamilyName}-before-"]:before
{
	font-family: "${process.arguments.prefix}${fontFamilyName}";
	-webkit-font-feature-settings: 'liga';

	font-style: normal !important;
	font-weight: normal !important;
	speak: none;

	display: block;
	text-decoration: none !important;
	text-align: center;
	/* width: 1em; */

	/* For safety - reset parent styles, that can break glyph codes*/
	font-variant: normal !important;
	text-transform: none !important;
	letter-spacing: normal !important;
	white-space: nowrap;
	word-wrap: normal;
	direction: ltr !important;

	/* fix buttons height, for twitter bootstrap */
	line-height: 1;

	/* you can be more comfortable with increased icons size */
	/* font-size: 120%; */

	/* Font smoothing. That was taken from TWBS */
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;

	/* Uncomment for 3D effect */
	/* text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3); */

}

`;
	let vars=`\n:root{\n`;
	for(let i=0; i<glyphs.length; i++){
		vars+=`\t--${fontFamilyName}-${glyphs[i].name}: "\\${glyphs[i].unicode[0].charCodeAt(0).toString(16).toUpperCase()}";\n`;
		css+=`.${fontFamilyName}-after-${glyphs[i].name}:after, .${fontFamilyName}-before-${glyphs[i].name}:before{ content: "\\${glyphs[i].unicode[0].charCodeAt(0).toString(16).toUpperCase()}"; /* '${glyphs[i].unicode[0]}' */ }\n`;
/* .${fontFamilyName}-inherit-inherit-inherit-after-${glyphs[i].name}>*>*>*:after,
.${fontFamilyName}-inherit-inherit-after-${glyphs[i].name}>*>*:after,
.${fontFamilyName}-inherit-after-${glyphs[i].name}>*:after, */
/* .${fontFamilyName}-inherit-inherit-inherit-${glyphs[i].name}>*>*>*:before,
.${fontFamilyName}-inherit-inherit-${glyphs[i].name}>*>*:before,
.${fontFamilyName}-inherit-${glyphs[i].name}>*:before, */
	}
	vars+='}';
	return css+vars;
}
return;
require('../_mods/argv.js').init();
require('../_mods/gulp.js');