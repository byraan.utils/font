const log=console.log,
chalk=require('chalk');
var colors={
	debug: 'gray',
	error: 'red',
	warn: '#ff5722',
	info: 'blue',
	success: 'green'
};
function color2chalk(color){
	let chalkBg, chalkColor;
	if(typeof color=='string'){
		if(color.charAt(0)=='#'){
			chalkBg=chalk.bgHex(color);
			chalkColor=chalk.hex(color);
		}
		else{
			chalkBg=chalk.bgKeyword(color);
			chalkColor=chalk.keyword(color);
		}
	}
	else if(typeof color=='object' && color.length==3){
		chalkBg=chalk.bgRgb(color[0], color[1], color[2]);
		chalkColor=chalk.rgb(color[0], color[1], color[2]);
	}
	else{
		chalkBg=false;
		chalkColor=false;
	};
	return {
		color: chalkColor,
		bg: chalkBg
	};
}
module.exports=function(){
	if(!arguments.length) return module.exports;
	for(let i=0; i<arguments.length; i++){
		let msg=arguments[i];
		let type,
		color=false,
		print;
		if(typeof msg=='object') msg=JSON.stringify(msg, null, '\t');
		else if(typeof msg!='string') msg=String(msg);
		type=msg.match(/^\{{2}[\w \s \- \_]*\}{2}/);
		if(type){
			msg=((msg.split(type))[1]).trim();
			type=(type[0].substr(2, type[0].length-4)).toLowerCase();
			color=colors[type];
			type=type.toUpperCase();
			if(color) print=color2chalk(color);
		}
		if(!msg){
			return function(){
				let args=[];
				for(let i=0; i<arguments.length; i++){
					let msg=arguments[i];
					if(typeof msg=='object') msg=JSON.stringify(msg, null, '\t');
					else if(typeof msg!='string') msg=String(msg);
					args.push(`${type?`{{${type}}}`:''}${msg}`);
				}
				module.exports.apply(null, args);
			}
		}
		if(!color){
			if(!type) type=' ';
			log(chalk.bgKeyword('white')(chalk.keyword('black')(` ${type} `)), chalk.keyword('white')(msg));
			return;
		}
		msg=msg.replace(/\n/g, '\n'+print.bg(' ')+' ');
		log(print.bg((` ${type} `)), print.color(msg));
	}
};