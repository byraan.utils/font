const {exec}=require('child_process'),
log=require('../lib/log');
module.exports=function(){
	var i=0,
	log_allow=this,
	args=arguments,
	resolver;
	var promise=new Promise(function(resolve, reject){
		resolver=resolve;
	});
	function exe(){
		line=args[i];
		let allow_log;
		exec(line, function(err, out, errout){
			// if(err) log('{{warn}}')(err);
			if(errout) log('{{warn}}')(errout);
			console.log(out);
			i++;
			if(i>=args.length){
				resolver();
				return;
			}
			exe();
		});
	};
	exe();
	return promise;
};