# Requerimientos

- NodeJS
- Archivos de iconos SVG

# Descripción

Objetivo: crear fuente de iconos para usar con HTML y CSS (alternativa a utilizar directamente archivos SVG)

- Crea un archivo de fuente que contiene *glyphs* creados a partir de archivos SVG
- Crea un archivo .css que contiene:
  - Regla css *@font-face* (opcional)
  - Selectores que usan la familia declarada con *@font-face* (css) o con *FontFace* (js)
  - CSS para obtener cada icono
    - Selectores
    - Variables (ej. *--IC-nombre_icono*)

# Instalar dependencias

```bash
npm install
npm link
```

# Uso

Bash
```bash
# Genera fuente llamada "nombre_paquete"
# Los iconos svg son obtenidos desde la carpeta "nombre_paquete" en la carpeta actual
utils-font icon "nombre_paquete"
```

HTML
```html
<span class="nombre_paquete-before-nombre_icono">icono antes del texto</span>
<span class="nombre_paquete-after-nombre_icono">icono después del texto</span>
```

CSS
```css
selector-elemento:before {
	font-family: 'nombre_paquete';
	content: var(--IC-nombre_icono);
}
```

# Personalizar

### Seleccionar carpeta contenedora de iconos svg

Obtener iconos desde la carpeta "ruta/contenedora/de/svg"

```bash
utils-font icon "nombre_paquete" -i "ruta/contenedora/de/svg"
```

### Seleccionar carpeta donde guardar archivos

```bash
utils-font icon "nombre_paquete" -o "ruta/de/salida"
```

## CSS

### Insertar CSS @font-face en el archivo CSS generado

```bash
utils-font icon "nombre_paquete" --css-rule
```

### Cambiar prefijo de la clase usada en selectores

```bash
utils-font icon "nombre_paquete" --class "clase_personalizada"
```

HTML
```html
<span class="clase_personalizada-before-nombre_icono"></span>
```

### Cambiar prefijo del nombre de la familia

```bash
utils-font icon "nombre_paquete" --prefix "custom_icons-"
```

CSS
```css
selector-elemento:before {
	font-family: 'custom_icons-nombre_paquete';
}
```

## Fuente

### Generar solo archivo de fuente

```bash
utils-font icon "nombre_paquete" --no-css
```

### Tipo de archivo de fuente

**Soporte**
- ttf
- eot
- **woff** (por defecto)
- woff2

```bash
utils-font icon "nombre_paquete" -f "ttf"
```